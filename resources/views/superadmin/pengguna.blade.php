@extends('superadmin.template.main')

@section('title', 'Data Pengguna - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-4" id="top-content">
                            <h6 class="card-title m-0">Data Pengguna</h6>
                            {{-- <div id="pesanupdate"></div> --}}

                            @if (session('success'))
                                <div id="success-message" class="alert alert-success">
                                    {{ session('success') }}
                                </div>

                                <script>
                                    // Menghilangkan pesan setelah 5 detik
                                    setTimeout(function() {
                                        document.getElementById('success-message').style.display = 'none';
                                    }, 5000); // 5000 milidetik = 5 detik
                                </script>
                            @endif
                            <div class="bt-group">
                                <button type="button" id="bt-tambah" class="btn btn-success btn-sm btn-icon-text"
                                    data-bs-toggle="modal" data-bs-target="#modalTambah">
                                    <i class="link-icon" data-feather="plus-square"></i> Tambah Data</button>
                                <button type="button" onclick="hapusData()" class="btn btn-danger btn-sm btn-icon-text"
                                    id="bt-del"><i class="link-icon" data-feather="x-square"></i> Hapus Data</button>
                            </div>
                        </div>
                        @if ($errors->any())
                            <div id="errorAlert" class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <script>
                                setTimeout(function() {
                                    document.getElementById('errorAlert').style.display = 'none';
                                }, 5000); // Menghilangkan pesan error setelah 5 detik (5000 milidetik)
                            </script>
                        @endif
                        <div class="table-responsive">
                            <table id="tabelPengguna" class="table hover stripe" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" name="" data-id="' + row.id +'"
                                                class="form-check-input check-all"></th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                {{-- <tbody>
                                    @foreach ($pengguna as $data)
                                        <tr>
                                            <td><input type="checkbox" class="form-check-input check"></td> --}}
                                {{-- <td>John Bayer</td> --}}
                                {{-- <td>{{ $data->email }}</td>
                                            <td><span class="badge bg-primary">{{ $data->role }}</span></td>
                                            <td><i class="link-icon" data-feather="check-circle" id="online"></i></td> --}}
                                {{-- <td><button type="button" id="bt-detail"
                                                class="btn btn-secondary btn-sm btn-icon-text"><i class="link-icon"
                                                    data-feather="eye" data-bs-toggle="modal"
                                                    data-bs-target="#modalDetail"></i> </button>
                                                <button type="button" id="bt-edit" class="btn btn-success btn-sm btn-icon-text"><i
                                                        class="link-icon" data-feather="edit" data-bs-toggle="modal"
                                                        data-bs-target="#modalEdit" onclick="#"></i> </button>
                                            </td>
                                        </tr> --}}
                                {{-- @endforeach --}}
                                {{-- </tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div id="modalTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTambahLabel"
        aria-hidden="true" data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTambahLabel">Tambah Pengguna Baru
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('pgnstore') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="emailPengguna" class="form-label">Email Pengguna</label>
                            <input type="email" class="form-control" id="emailPengguna" name="email"
                                value="{{ old('email') }}" placeholder="Masukkan Email Pengguna">
                            @error('email')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="namaLengkap" class="form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="namaLengkap" name="nama_lengkap"
                                value="{{ old('nama_lengkap') }}" placeholder="Masukkan Nama Lengkap">
                            @error('nama_lengkap')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="noTelp" class="form-label">No Telepon</label>
                            <input type="text" class="form-control" id="noTelepon" name="no_telepon"
                                value="{{ old('no_telepon') }}" placeholder="Masukkan Nama Lengkap">
                            @error('no_telepon')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>


                        <div class="mb-3">
                            <label for="passwordPengguna" class="form-label">Password Pengguna</label>
                            <input type="password" class="form-control" id="passwordPengguna" name="password"
                                placeholder="Masukkan Password Pengguna">
                            @error('password')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="role" class="form-label">Role Pengguna</label>
                            <select class="form-control" id="role" name="role">
                                <option>Pilih Role Pengguna</option>
                                <option value="Super Admin">Super Admin</option>
                                <option value="Admin">Admin</option>
                                <option value="Teknisi">Teknisi</option>
                            </select>
                            @error('role')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="status" class="form-label">Status Pengguna</label>
                            <select class="form-control" id="status" name="status">
                                <option>Pilih Status Pengguna</option>
                                <option value="Aktif">Aktif</option>
                                <option value="Tidak Aktif">Tidak Aktif</option>
                            </select>
                            @error('status')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" id="btnSimpan" class="btn btn-success">Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Edit Pengguna</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formEdit" method="POST">
                        {{-- <form action="{{ url('superadmin/'. $data->id) }}" method="POST">  --}}
                        @csrf
                        @method('PUT')
                        {{-- <input type="text" name="id" value="{{ $data->id }}"> --}}
                        <div class="mb-3">
                            <label for="emailPengguna" class="form-label">Email Pengguna</label>
                            <input type="email" class="form-control" id="emailPenggunaEdit" name="email"
                                {{-- value="{{ $data->email }}" --}} placeholder="Masukkan Email Pengguna">
                        </div>
                        <div class="mb-3">
                            <label for="passwordPengguna" class="form-label">Password Pengguna</label>
                            <input type="password" class="form-control" id="passwordPenggunaEdit" name="password"
                                placeholder="Masukkan Password Pengguna">
                            @error('password')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="role" class="form-label">Role Pengguna</label>
                            <select class="form-control" id="roleEdit" name="role">
                                <option selected disabled>Pilih Role Pengguna</option>
                                <option value="Super Admin">Super Admin</option>
                                <option value="Admin">Admin</option>
                                <option value="Teknisi">Teknisi</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="status" class="form-label">Status Pengguna</label>
                            <select class="form-control" id="statusEdit" name="status">
                                <option selected disabled>Pilih Status Pengguna</option>
                                <option value="Aktif">Aktif</option>
                                <option value="Tidak Aktif">Tidak Aktif</option>
                            </select>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="id" name="id">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modalDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalDetailLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel">Detail Pengguna</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="detailEmail" class="form-label">Email:</label>
                            <input type="text" class="form-control" id="detailEmail" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailRole" class="form-label">Role:</label></label>
                            <input type="text" class="form-control" id="detailRole" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="detailStatus" class="form-label">Status:</label>
                            <input type="email" class="form-control" id="detailStatus" readonly>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script type="text/javascript">
        const checkAllCheckbox = document.querySelector('.check-all');
        const checkboxes = document.querySelectorAll('.check');

        checkAllCheckbox.addEventListener('change', function() {
            checkboxes.forEach(function(checkbox) {
                checkbox.checked = checkAllCheckbox.checked;
            });
        });

        $(document).ready(function() {
            $('#modalTambah, #modalEdit').on('hidden.bs.modal', function() {
                $(this).find('input[type=text], input[type=email]').val('');
                $(this).find('select').val('Pilih Role Pengguna');
            });

            @if ($errors->any())
                $('#modalTambah').modal('show');
            @endif
        });


        var tabel;
        // read data pengguna
        $(document).ready(function() {
            tabel = $('#tabelPengguna').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('datapengguna') }}",
                columns: [{
                        data: 'id',
                        name: 'id',
                        render: function(data, type, row, meta) {
                            return '<input type="checkbox" name="dataHapus[]" class="form-check-input check ceklis" value="' +
                                data + '" data-id="' + data + '">';
                        },
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'role',
                        name: 'role',
                        render: function(data, type, row, meta) {
                            var badgeClass = data === 'Admin' ? 'bg-secondary' : (data ===
                                'Teknisi' ? 'bg-info' : 'bg-primary');
                            return '<span class="badge ' + badgeClass + '">' + data + '</span>';
                        }
                    },
                    {
                        data: 'status',
                        name: 'status',
                        render: function(data, type, row, meta) {
                            let iconColor = data === 'Aktif' ? 'style="color: green;"' :
                                'style="color: red;"';
                            return `<i class="link-icon" data-feather="check-circle" id="online" ${iconColor}></i>`;
                        }
                    },
                    {
                        data: null,
                        name: 'aksi',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return `<button type="button" onclick="modalDetail('${row.email}','${row.role}','${row.status}')"class="btn btn-secondary btn-sm btn-icon-text"><i 
                                                class="link-icon" data-feather="eye" data-bs-toggle="modal"
                                                    data-bs-target="#modalDetail"></i> </button>
                                    <button type="button" onclick="modalEdit('${row.id}','${row.email}','${row.role}','${row.status}')" class="btn btn-success btn-sm btn-icon-text"><i
                                                class="link-icon" data-feather="edit" data-bs-toggle="modal"
                                                    data-bs-target="#modalEdit" onclick="#"></i> </button>`;
                        }
                    }
                ],
                aLengthMenu: [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                iDisplayLength: 10,
                language: {
                    search: "",
                    paginate: {
                        previous: "Sebelumnya",
                        next: "Selanjutnya"
                    },
                    info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    search: "Cari:",
                    lengthMenu: "Tampilkan _MENU_ entri",
                    zeroRecords: "Tidak ditemukan data yang sesuai",
                    infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                    infoFiltered: "(disaring dari _MAX_ entri keseluruhan)"
                },
                responsive: true,
                drawCallback: function(settings) {
                    feather.replace();

                    // $('#formEdit').on('submit', function(e) {
                    //     e.preventDefault();
                    //     let data = $(this).serialize();

                    //     $.ajax({
                    //         url: "{{ route('pgn.update') }}",
                    //         type: "POST",
                    //         data: data,
                    //         success: function(response) {
                    //             console.log(response);
                    //             Swal.fire({
                    //                 title: 'Berhasil',
                    //                 text: 'Data berhasil diubah',
                    //                 icon: 'success',
                    //                 confirmButtonText: 'OK'
                    //             });
                    //             $('#modalEdit').modal('hide');
                    //             tabel.ajax.reload();
                    //         },
                    //         error: function(xhr) {
                    //             console.log(xhr.responseJSON.message);
                    //             Swal.fire({
                    //                 title: 'Gagal',
                    //                 text: xhr.responseJSON.message,
                    //                 icon: 'error',
                    //                 confirmButtonText: 'OK'
                    //             });

                    //         }
                    //     });
                    // });

                    // function modalEdit(id, email, role, status) {
                    //     $('#id').val(id);
                    //     $('#emailPenggunaEdit').val(email);
                    //     $('#roleEdit').val(role);
                    //     $('#statusEdit').val(status);
                    //     $('#modalEdit').modal('show');
                    // }

                    // function modalDetail(email, role, status) {
                    //     $('#detailEmail').val(email);
                    //     $('#detailRole').val(role);
                    //     $('#detailStatus').val(status);
                    //     $('#modalDetail').modal('show');
                    // }

                    // $(document).on('click', '.detail-btn', function() {
                    //     var email = $(this).data('email');
                    //     var role = $(this).data('role');
                    //     var status = $(this).data('status');

                    //     modalDetail(email, role, status);
                    // });
                },
                initComplete: function() {
                    feather.replace();
                }
            });

            $('#tabelPengguna').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Cari');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });

            $('.check-all').on('change', function() {
                var isChecked = $(this).is(':checked');
                $('.check').prop('checked', isChecked);
            });

            tabel.on('responsive-display.dt', function(e, datatable, row, showHide, update) {
                feather.replace();
            });
        });

        // hapus data pengguna
        function hapusData() {
            var dataHapus = [];
            $('.ceklis:checked').each(function() {
                dataHapus.push($(this).val());
            });

            if (dataHapus.length > 0) {
                Swal.fire({
                    title: 'Anda yakin?',
                    text: 'Data yang dihapus tidak dapat dikembalikan!',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: 'Ya, hapus!',
                    cancelButtonText: 'Batal'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: "{{ route('pgn.delete') }}",
                            type: "DELETE",
                            data: {
                                dataHapus: dataHapus,
                                _token: "{{ csrf_token() }}",
                                _method: "DELETE"
                            },
                            success: function(response) {
                                console.log(response);
                                Swal.fire({
                                    title: 'Berhasil',
                                    text: 'Data berhasil dihapus',
                                    icon: 'success',
                                    confirmButtonText: 'OK'
                                });
                                tabel.ajax.reload();
                            },
                            error: function(xhr) {
                                console.log(xhr.responseJSON.message);
                                Swal.fire({
                                    title: 'Gagal',
                                    text: xhr.responseJSON.message,
                                    icon: 'error',
                                    confirmButtonText: 'OK'
                                });
                            }
                        });
                    }
                });
            } else {
                Swal.fire({
                    title: 'Peringatan',
                    text: 'Pilih data yang akan dihapus',
                    icon: 'warning',
                    confirmButtonText: 'OK'
                });
            }
        }


        $('#formEdit').on('submit', function(e) {
            e.preventDefault();
            let data = $(this).serialize();

            $.ajax({
                url: "{{ route('pgn.update') }}",
                type: "POST",
                data: data,
                success: function(response) {
                    console.log(response);
                    Swal.fire({
                        title: 'Berhasil',
                        text: 'Data berhasil diubah',
                        icon: 'success',
                        confirmButtonText: 'OK'
                    });
                    $('#modalEdit').modal('hide');
                    tabel.ajax.reload();
                },
                error: function(xhr) {
                    console.log(xhr.responseJSON.message);
                    Swal.fire({
                        title: 'Gagal',
                        text: xhr.responseJSON.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    });

                }
            });
        });

        function modalEdit(id, email, role, status) {
            $('#id').val(id);
            $('#emailPenggunaEdit').val(email);
            $('#roleEdit').val(role);
            $('#statusEdit').val(status);
            $('#modalEdit').modal('show');
        }

        function modalDetail(email, role, status) {
            $('#detailEmail').val(email);
            $('#detailRole').val(role);
            $('#detailStatus').val(status);
            $('#modalDetail').modal('show');
        }

        $(document).on('click', '.detail-btn', function() {
            var email = $(this).data('email');
            var role = $(this).data('role');
            var status = $(this).data('status');

            modalDetail(email, role, status);
        });

        $(window).resize(function() {
            $('#tabelPengguna').DataTable().columns.adjust().responsive.recalc();
        });
    </script>
@endpush

@push('style')
    <style>
        .btn-secondary {
            margin-right: 5px;
        }

        .link-icon {
            max-width: 20px;
        }

        #bt-tambah {
            margin-right: 10px;
        }

        #online {
            color: greenyellow;
        }

        #offline {
            color: gray;
        }

        #tabelPengguna thead th:first-child {
            cursor: default;
        }

        #tabelPengguna thead th:first-child::after,
        #tabelPengguna thead th:first-child::before {
            display: none !important;
            pointer-events: none;
        }

        #tabelPengguna td,
        #tabelPengguna th {
            text-align: center;
        }

        #tabelPengguna td.child {
            text-align: left;
        }

        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        .dataTables_empty {
            text-align: center !important;
        }

        @media only screen and (max-width: 768px) {
            #tabelPengguna_filter {
                margin-top: 10px;
            }

            #tabelPengguna td {
                white-space: normal;
                word-wrap: break-word;
            }
        }

        @media only screen and (max-width: 556px) {
            #top-content {
                flex-direction: column;
            }

            .bt-group {
                flex-direction: column;
            }

            #bt-tambah {
                width: 100%;
                margin-top: 10px;
            }

            #bt-del {
                width: 100%;
                margin-top: 10px;
            }

        }
    </style>
@endpush
