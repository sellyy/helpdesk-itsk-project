@extends('admin.template.main')

@section('title', 'Data Aduan Karyawan - Helpdesk ITSK')

@section('content')
    <div class="page-content mt-n4">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Data Laporan Pengaduan Karyawan</h6>
                        <div class="table-responsive">
                            <table id="TabelTiketKaryawan" class="table hover stripe" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>No. Tiket</th>
                                        <th>Email</th>
                                        <th>No. Telp</th>
                                        <th>NIK</th>
                                        <th>Waktu</th>
                                        <th>Teknisi</th>
                                        <th>Urgensi</th>
                                        <th>Unit Kerja</th>
                                        <th>Judul Tiket</th>
                                        <th>Kategori</th>
                                        <th>Kategori (Jika Memilih 'Lainnya')</th>
                                        <th>Deskripsi</th>
                                        <th>File Tiket</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div id="modalProses" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalProsesLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProsesLabel">Pilih Teknisi
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="teknisi" class="form-label">Teknisi</label>
                            <select class="form-control select2" id="teknisi" name="teknisi">
                                <option hidden value="">Pilih Teknisi</option>
                                @foreach ($namaPekerjaTeknisi as $nama)
                                    <option value="{{ $nama->id_pengguna }}">{{ $nama->nama_lengkap }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success" id="btn-simpan" data-tiket-id=""><i
                                class="fas fa-save"></i> Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalImage" tabindex="-1" aria-labelledby="modalImageLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalImageLabel">File Image</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img id="modalImageContent" src="" alt="File Image" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        function openModal(imageSrc) {
            document.getElementById('modalImageContent').src = imageSrc;
            var modal = new bootstrap.Modal(document.getElementById('modalImage'));
            modal.show();
        }

        var tabelTiketKaryawan;
        $(document).ready(function() {
            tabelTiketKaryawan = $('#TabelTiketKaryawan').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('admtiketkaryawan') }}",
                "columns": [{
                        "data": null
                    },
                    {
                        "data": "nama"
                    },
                    {
                        "data": "no_tiket"
                    },
                    {
                        "data": "email"
                    },
                    {
                        "data": "no_telepon",
                        render: function(data, type, row, meta) {
                            return '<a href="https://wa.me/' + data + '" target="_blank">' + data +
                                '</a>';
                        }
                    },
                    {
                        "data": "nik_karyawan"
                    },
                    {
                        "data": "waktu_tiket"
                    },
                    {
                        "data": "nama_lengkap"
                    },
                    {
                        "data": "tingkat_urgensi",
                        render: function(data, type, row, meta) {
                            var badgeClass = data == 'Rendah' ? 'bg-success' : (data ==
                                'Sedang' ? 'bg-warning' : 'bg-danger');
                            return '<span class="badge ' + badgeClass + '">' + data + '</span>';
                        }
                    },
                    {
                        "data": "unit_kerja_id"
                    },
                    {
                        "data": "judul_tiket"
                    },
                    {
                        "data": "kategori_laporan"
                    },
                    {
                        "data": "kategori_lainnya"
                    },
                    {
                        "data": "deskripsi_tiket"
                    },
                    {
                        "data": "file_tiket",
                        render: function(data, type, row, meta) {
                            if (data !== '---') {
                                return '<button type="button" class="btn btn-secondary btn-sm btn-icon-text" onclick="openModal(\'/storage/' +
                                    data + '\')">Lihat File</button>';
                            } else {
                                return '---';
                            }
                        }
                    },
                    {
                        "data": "status_tiket",
                        render: function(data, type, row, meta) {
                            var badgeClass = data === 'Selesai' ? 'bg-success' : (data ===
                                'Sedang Diproses' ? 'bg-warning' : 'bg-danger');
                            return '<span class="badge ' + badgeClass + '">' + data + '</span>';
                        }
                    },
                    {
                        "data": "aksi",
                        render: function(data, type, row, meta) {
                            if (data === 'Button') {
                                return `<button type="button" id="bt-proses" class="btn btn-success btn-sm btn-icon-text" data-bs-toggle="modal" data-bs-target="#modalProses" data-tiket-id="` +
                                    row.id + `">Kerjakan</button>`;
                            } else {
                                return data;
                            }
                        }
                    }
                ],
                "aLengthMenu": [
                    [10, 30, 50, -1],
                    [10, 30, 50, "All"]
                ],
                "iDisplayLength": 10,
                "order": [
                    [6, "desc"],
                    [15, "asc"]
                ],
                "language": {
                    search: "",
                    "paginate": {
                        "previous": "Sebelumnya",
                        "next": "Selanjutnya"
                    },
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "search": "Cari:",
                    "lengthMenu": "Tampilkan _MENU_ entri",
                    "zeroRecords": "Tidak ditemukan data yang sesuai",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                    "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)"
                },
                "responsive": true,
                "columnDefs": [{
                    "orderable": false,
                    "targets": 0
                }]
            });

            $('#TabelTiketKaryawan').DataTable().on('order.dt search.dt', function() {
                $('#TabelTiketKaryawan').DataTable().column(0, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

            $('#TabelTiketKaryawan').each(function() {
                var datatable = $(this);
                var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
                search_input.attr('placeholder', 'Search');
                search_input.removeClass('form-control-sm');
                var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
                length_sel.removeClass('form-control-sm');
            });
        });

        $('#modalProses').on('hidden.bs.modal', function(e) {
            $('#teknisi').val('');
        });

        $(document).on('click', '#bt-proses', function(event) {
            event.preventDefault();

            $('#modalProses').modal('show');
            var button = $(event.currentTarget);
            var tiketId = button.data('tiket-id');

            $('#btn-simpan').attr('data-tiket-id', tiketId);
        });

        $('#btn-simpan').click(function(event) {
            event.preventDefault();
            var tiketId = $(this).data('tiket-id');
            var idPengguna = $('#teknisi').val();

            $.ajax({
                type: 'POST',
                url: '/kirim-tiket-karyawan',
                data: {
                    _token: '{{ csrf_token() }}',
                    tiketId: tiketId,
                    idPengguna: idPengguna
                },
                success: function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil! 👌🥳',
                        text: response.message,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#modalProses').modal('hide');
                        }
                    });
                    tabelTiketKaryawan.ajax.reload();
                },
                error: function(xhr, status, error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal! ✋😌',
                        text: 'Pilih Teknisi!',
                        confirmButtonText: 'OK'
                    });
                }
            });
        });
    </script>
@endpush

@push('style')
    <style>
        #TabelTiketKaryawan th,
        #TabelTiketKaryawan td {
            text-align: left;
        }

        .page-item.active .page-link {
            background-color: #14A44D !important;
            border-color: #14A44D !important;
            color: white !important;
        }

        .page-link {
            color: #333333 !important;
        }

        .dataTables_empty {
            text-align: center !important;
        }

        #TabelTiketKaryawan td,
        #TabelTiketKaryawan th {
            text-align: center;
        }

        #TabelTiketKaryawan td.child {
            text-align: left;
        }

        @media only screen and (max-width: 768px) {
            #TabelTiketKaryawan td {
                white-space: normal;
                word-wrap: break-word;
            }

            #TabelTiketKaryawan_filter {
                margin-top: 10px;
            }
        }
    </style>
@endpush
