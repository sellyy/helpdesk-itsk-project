<?php

namespace App\Http\Controllers\Teknisi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Carbon\Carbon;
use App\Models\Tiket;
use Illuminate\Support\Facades\Auth;
use App\Models\RiwayatTiket;

class DownloadController extends Controller
{
    public function generateExcel(Request $request)
    {
        $date = $request->input('date');
        $formattedDate = date('Y-m-d', strtotime($date));

        $data = Tiket::whereDate('waktu_tiket', $formattedDate)->get()->map(function ($tiket) {
            foreach ($tiket->getAttributes() as $key => $value) {
                if ($value === null) {
                    $tiket->{$key} = '---';
                }
            }

            if ($tiket->nip_dosen !== '---') {
                $tiket->posisi = 'Dosen';
            } elseif ($tiket->nim_mahasiswa !== '---') {
                $tiket->posisi = 'Mahasiswa';
            } elseif ($tiket->nik_karyawan !== '---') {
                $tiket->posisi = 'Karyawan';
            } else {
                $tiket->posisi = 'Tidak Diketahui';
            }

            $tiket->waktu_tiket = Carbon::parse($tiket->waktu_tiket)->format('d/m/Y');

            return $tiket;
        });

        return (new FastExcel($data))->download('report.xlsx', function ($tiket) {
            return [
                'ID' => $tiket->id,
                'Nama' => $tiket->nama,
                'Posisi' => $tiket->posisi,
                'No. Tiket' => $tiket->no_tiket,
                'Email' => $tiket->email,
                'No. Telp' => $tiket->no_telepon,
                'Prodi' => $tiket->prodi,
                'Unit Kerja' => $tiket->unit_kerja_karyawan,
                'NIP' => $tiket->nip_dosen,
                'NIM' => $tiket->nim_mahasiswa,
                'NIK' => $tiket->nik_karyawan,
                'Judul Tiket' => $tiket->judul_tiket,
                'Urgensi' => $tiket->tingkat_urgensi,
                'Kategori' => $tiket->kategori_laporan,
                'Kategori (Jika Memilih "Lainnya")' => $tiket->kategori_lainnya,
                'Deskripsi' => $tiket->deskripsi_tiket,
                'Waktu' => $tiket->waktu_tiket,
                'Status' => $tiket->status_tiket,
            ];
        });
    }

    public function downloadExcelTiketBelumProses(Request $request)
    {
        $date = $request->input('date');
        $teknisiId = Auth::id();
        $formattedDate = date('Y-m-d', strtotime($date));

        $tiketBelumProses = Tiket::where('id_pengguna', $teknisiId)
            ->whereDate('tanggal_masuk', $formattedDate)
            ->where('status_tiket', 'Belum Diproses')
            ->with('lampiran')
            ->get();

        foreach ($tiketBelumProses as $tiket) {
            if ($tiket->nip_dosen !== null) {
                $tiket->posisi = 'Dosen';
            } elseif ($tiket->nim_mahasiswa !== null) {
                $tiket->posisi = 'Mahasiswa';
            } elseif ($tiket->nik_karyawan !== null) {
                $tiket->posisi = 'Karyawan';
            } else {
                $tiket->posisi = 'Tidak Diketahui';
            }

            if ($tiket->kategori_laporan === 'Lainnya') {
                $tiket->kategori_laporan = $tiket->kategori_lainnya;
            }
        }

        $tiketBelumProses->each(function($item) {
            $item->file_tiket = $item->lampiran->first()->file_tiket ? 'Ada' : 'Tidak Ada';
        });

        return (new FastExcel($tiketBelumProses))->download('report.xlsx', function ($tiket) use ($date, &$no) {
            $no++;
            return [
                'NO' => $no,
                'Nama' => $tiket->nama,
                'Posisi' => $tiket->posisi,
                'No Telepon' => $tiket->no_telepon,
                'Kategori Laporan' => $tiket->kategori_laporan,
                'Judul Tiket' => $tiket->judul_tiket,
                'Tanggal Masuk' => $date,
                'File Tiket' => $tiket->file_tiket,
            ];
        });
    }

    public function generateExcelProses(Request $request)
    {
        $date = $request->input('date');
        $formattedDate = date('Y-m-d', strtotime($date));

        $data = Tiket::whereDate('tanggal_pengerjaan', $formattedDate)->where('status_tiket', 'Sedang Diproses')->whereHas('riwayat', function ($query) {
            $query->where('deskripsi_riwayat', 'laporan sedang ditangani teknisi');
        })->with('lampiran')->get()->map(function ($tiket) {
            foreach ($tiket->getAttributes() as $key => $value) {
                if ($value === null) {
                    $tiket->{$key} = '---';
                }
            }

            $tiket->file_tiket = $tiket->lampiran->first()->file_tiket ? 'Ada' : 'Tidak Ada';

            if ($tiket->nip_dosen !== '---') {
                $tiket->posisi = 'Dosen';
            } elseif ($tiket->nim_mahasiswa !== '---') {
                $tiket->posisi = 'Mahasiswa';
            } elseif ($tiket->nik_karyawan !== '---') {
                $tiket->posisi = 'Karyawan';
            } else {
                $tiket->posisi = 'Tidak Diketahui';
            }

            $tiket->tanggal_pengerjaan = Carbon::parse($tiket->tanggal_pengerjaan)->format('d/m/Y');

            return $tiket;
        });

        return (new FastExcel($data))->download('report.xlsx', function ($tiket) use (&$no) {
            // $riwayat = RiwayatTiket::where('id_tiket', $tiket->id)->where('deskripsi_riwayat', 'laporan sedang ditangani teknisi')->first();
            // $riwayat->estimasi_selesai = Carbon::parse($riwayat->estimasi_selesai)->format('d/m/Y');
            $tiket->estimasi_selesai = Carbon::parse($tiket->estimasi_selesai)->format('d/m/Y');
            $no++;
            return [
                'No' => $no,
                'Nama' => $tiket->nama,
                'Posisi' => $tiket->posisi,
                'No. Telp' => $tiket->no_telepon,
                'Kategori Masalah' => $tiket->kategori_laporan ?? $tiket->kategori_lainnya,
                'Deskripsi Masalah' => $tiket->deskripsi_tiket,
                'Tanggal Mulai' => $tiket->tanggal_pengerjaan,
                'Estimasi Selesai' => $tiket->estimasi_selesai,
                'File Tiket' => $tiket->file_tiket,
            ];
        });
    }

    public function generateExcelSelesai(Request $request)
    {
        $date = $request->input('date');
        $formattedDate = date('Y-m-d', strtotime($date));

        $data = Tiket::whereDate('tanggal_masuk', $formattedDate)->where('status_tiket', 'Selesai')->with('lampiran')->get()->map(function ($tiket) {
            foreach ($tiket->getAttributes() as $key => $value) {
                if ($value === null) {
                    $tiket->{$key} = '---';
                }
            }

            if ($tiket->nip_dosen !== '---') {
                $tiket->posisi = 'Dosen';
            } elseif ($tiket->nim_mahasiswa !== '---') {
                $tiket->posisi = 'Mahasiswa';
            } elseif ($tiket->nik_karyawan !== '---') {
                $tiket->posisi = 'Karyawan';
            } else {
                $tiket->posisi = 'Tidak Diketahui';
            }

            $tiket->file_tiket = $tiket->lampiran->first()->file_tiket ? 'Ada' : 'Tidak Ada';

            $tiket->tanggal_masuk = Carbon::parse($tiket->tanggal_masuk)->format('d/m/Y');

            return $tiket;
        });

        return (new FastExcel($data))->download('report.xlsx', function ($tiket) use (&$no) {
            $riwayatTiket = RiwayatTiket::where('deskripsi_riwayat', 'Laporan sudah selesai')->first();
            $riwayatTiket->waktu_riwayat = Carbon::parse($riwayatTiket->waktu_riwayat)->format('d/m/Y');
            $no++;
            return [
                'No' => $no,
                'Nama' => $tiket->nama,
                'Posisi' => $tiket->posisi,
                'No. Telp' => $tiket->no_telepon,
                'Kategori Masalah' => $tiket->kategori_laporan ?? $tiket->kategori_lainnya,
                'Deskripsi Masalah' => $tiket->deskripsi_tiket,
                'Tanggal Masuk' => $tiket->tanggal_masuk,
                'Tanggal Selesai' => $riwayatTiket->waktu_riwayat,
                'File Tiket' => $tiket->file_tiket,
            ];
        });
    }
}
